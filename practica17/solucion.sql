﻿USE tienda;

-- CONSULTA 4

-- c1
SELECT MIN(p.precio) minimo FROM producto p;

-- OPCION 1
-- c2
SELECT * 
FROM producto p 
WHERE p.precio=(SELECT MIN(p.precio) minimo FROM producto p);

-- final

SELECT c2.nombre,f.nombre,c2.precio 
FROM fabricante f 
JOIN (
  SELECT * 
  FROM producto p 
  WHERE p.precio=(SELECT MIN(p.precio) minimo FROM producto p)
) c2 ON c2.codigo_fabricante=f.codigo;


-- OPCION 2 
SELECT f.nombre,p.nombre,p.precio 
FROM producto p 
JOIN fabricante f ON p.codigo_fabricante = f.codigo
WHERE p.precio=(SELECT MIN(p.precio) minimo FROM producto p);
