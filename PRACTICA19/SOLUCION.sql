﻿/**
  PRACTICA 19
 **/


USE rubros;


-- como se que voy a realizar consultas de accion
-- desactivo el autocommit

set AUTOCOMMIT=0;

/** 
  EJERCICIO 1
  PRODUCTOS se detallan los diferentes productos que se comercializan. 
  Insertar un campo rotulada como Desc-Rubro y en esta mostrar el tipo de rubro, 
  dependiendo de la información detallada en la tabla Rubros.
 **/

  -- EL CAMPO YA ESTA CREADO PERO MAL TIPADO
  -- 1 OPCION ME PERMITE CAMBIAR NOMBRE DEL CAMPO Y TIPO
  -- ALTER TABLE productos CHANGE COLUMN `DESC-RUBRO` `DESC-RUBRO` varchar(20); 
  
  -- 2 OPCION ME PERMITA CAMBIAR EL TIPO PERO NO EL NOMBRE
  ALTER TABLE productos MODIFY COLUMN `DESC-RUBRO` varchar(20);

  -- consulta de seleccion para comprobar de donde saco los datos
    SELECT * FROM productos p JOIN rubros r USING(`COD-RUBRO`);

 -- realizo la consulta de actualizacion
 
 UPDATE productos p JOIN rubros r ON p.`COD-RUBRO` = r.`COD-RUBRO`
  SET p.`DESC-RUBRO`=r.`DESC-RUBRO`;

 -- necesito realizar el commit 
 COMMIT;


  /**
    EJERCICIO 2A
    En ésta, se deberá mostrar para cada columna de código su descripción correspondiente 
    (por ejemplo para Cod-Producto, se deberá mostrar Desc-Producto, etc). 
    Tener en cuenta que la información requerida se localiza en las hojas respectivas
  **/

    -- primero voy a actualizar los campos desc-rubro, cod-rubro y desc-producto de precios con
    -- la tabla productos
    
    -- CONSULTA DE SELECCION
    SELECT * FROM PRECIOS JOIN productos p ON precios.`COD-PRODUCTO` = p.`COD-PRODUCTO`;

    -- consulta de actualizacion
    UPDATE precios JOIN productos USING(`COD-PRODUCTO`)
      SET 
        precios.`DESC-PRODUCTO`=productos.`DESC-PRODUCTO`,
        precios.`COD-RUBRO`=productos.`COD-RUBRO`,
        precios.`DESC-RUBRO`=productos.`DESC-RUBRO`;

  -- commit de la actualizacion
  COMMIT;

  -- voy a actualizar el campo desc-marca 
  -- de la tabla precios desde la tabla marcas

    -- consulta de seleccion
      SELECT * FROM precios p JOIN marcas m USING(`COD-MARCA`);

  -- consulta de actualizacion
  UPDATE precios join marcas USING(`COD-MARCA`)
    SET precios.`DESC-MARCA`=marcas.`DESC-MARCA`;

-- commit
COMMIT;

   /**
    EJERCICIO 2B
    En un campo rotulada como COD-ARTICULO, deberá generar un código que 
    identifique a un producto por su marca. 
    Este se formará de unir el Cod-Producto, mas una “p” (de producto), 
    mas el Cod-Marca, mas una “m” (de marca) (ej: 1p4m).
    **/

  -- modificar el campo cod-articulo
  ALTER TABLE precios MODIFY COLUMN `COD-ARTICULO` char(8);

  -- CONSULTA DE SELECCION
    SELECT CONCAT(p.`COD-PRODUCTO`,"P",p.`COD-MARCA`,"M"),p.`COD-PRODUCTO`,p.`COD-MARCA`  
    FROM precios p;

 -- consulta de actualizacion
 UPDATE precios p
  SET p.`COD-ARTICULO`=CONCAT(p.`COD-PRODUCTO`,"P",p.`COD-MARCA`,"M");

COMMIT;

  /**
      EJERCICIO 2C
      En la columna IVA INCL. se deberá calcular el precio del artículo con su iva incluido.
      Tener en cuenta que cada rubro al cual pertenecen los artículos posee un iva específico, 
      dato que se localiza en la hoja Rubros.
    **/

      -- CONSULTA DE DEFINICION DE DATOS
      ALTER TABLE precios MODIFY COLUMN `IVA_Incl.` float;

      -- CONSULTA DE SELECCION
      SELECT r.IVA,IMPORTE,IMPORTE*r.IVA 
      FROM precios JOIN rubros r USING(`COD-RUBRO`);

    -- consulta de actualizacion
    UPDATE precios p JOIN rubros r USING(`COD-RUBRO`)
      SET p.`IVA_Incl.`=p.IMPORTE*r.IVA;

    COMMIT;

    /**
      EJERCICIO 3A
      En la columna COD-ARTICULO, deberá formar el mismo código que el generado en la hoja Precios. 
      Dicho dato le servirá para mostrar de forma automática el Precio Unit. (detallado como Importe en la hoja Precios) 
      de cada articulo.
    **/

      -- MODIFICAR EL CAMPO COD-ARTICULO MEDIANTE UNA CONSULTA DE DEFINICION DE DATOS

      ALTER TABLE gestion 
        MODIFY COLUMN  `COD-ARTICULO` char(10);

      -- consulta de actualizacion
      UPDATE gestion g
        SET g.`COD-ARTICULO`=CONCAT(g.`COD-PRODUCTO`,"p",g.`COD-MARCA`,"m");

      COMMIT;

      /**
        EJERCICIO 3B
        Rellenar el precio unitario en gestion desde la tabla precios
        Calcular la columna IMPORTE de la tabla gestion dato 
        que surge del precio unitario * la cantidad.
       **/

      -- Relleno el precio unitario
      UPDATE gestion g JOIN precios p USING(`COD-ARTICULO`)
        SET g.`PRECIO_UNIT.`=p.IMPORTE;

      COMMIT ;

      -- relleno el importe de gestion 
      UPDATE gestion g
        SET g.IMPORTE=g.CANTIDAD*g.`PRECIO_UNIT.`;

      COMMIT;

      /** 
        EJERCICIO 3C
        Mostrar el importe correspondiente al cálculo de la columna DESCUENTO, 
        teniendo en cuenta que se realizará un descuento en la venta de los artículos, 
        dependiendo del rubro y del día de la semana que se realice la misma. 
        Los días lunes poseen descuento los fiambres, 
        los días miércoles los lácteos y 
        los días viernes los productos de panadería. 
        El porcentaje se localiza en la hoja Rubros.
       **/

      -- voy a calcular el descuento en la tabla gestion
      -- sin tener en cuenta las condiciones
      UPDATE gestion g 
        JOIN precios p USING(`COD-ARTICULO`)
        JOIN rubros r USING(`COD-RUBRO`)
        SET g.DESCUENTO=r.DESCUENTO;

    -- voy a calcular el descuento teniendo en cuenta las condiciones exigidas
    -- dayofweek ==> dia de la semana
    -- if ==> para evaluar condiciones

      UPDATE gestion g 
        JOIN precios p USING(`COD-ARTICULO`)
        JOIN rubros r USING(`COD-RUBRO`)
        SET g.DESCUENTO=IF(
                            (DAYOFWEEK(g.FECHA)=2 AND r.`DESC-RUBRO`='fiambres')
                            OR
                            (DAYOFWEEK(g.FECHA)=4 AND r.`DESC-RUBRO`='lacteos')
                            OR
                            (DAYOFWEEK(g.FECHA)=6 AND r.`DESC-RUBRO`='panaderia')
                           ,
                           r.DESCUENTO, -- si cumple alguna condicion
                           0); -- si no cumple ninguna 

      COMMIT;

     /*
     EJERCICIO 3D
     En el SUBTOTAL, se deberá mostrar el importe de la venta, 
     considerando el descuento correspondiente.
     **/

    UPDATE gestion g
      SET g.SUBTOTAL=g.IMPORTE*(1-g.DESCUENTO);

  COMMIT ;

    /*  
      EJERCICIO 3E
      En el IVA, se deberá mostrar el importe correspondiente al cálculo del iva, 
      tomando en cuenta los porcentajes correspondientes de la hoja Rubros.
     **/

    UPDATE gestion g 
      JOIN precios p USING(`COD-ARTICULO`)
      JOIN rubros r using(`COD-RUBRO`)
      set g.IVA=r.IVA;
  
    COMMIT;

    /*
    EJERCICIO 3F
    Obtener el TOTAL, considerando el subtotal y el iva,
    */

    UPDATE gestion g
      SET g.TOTAL=g.SUBTOTAL*(1+g.IVA);  

    /**
    EJERCICIO 3G
    Mostrar las descripciones para las columnas PRODUCTO, RUBRO y MARCA.
    **/

  -- cogiendo los datos desde precios

  UPDATE gestion g JOIN precios p USING(`COD-ARTICULO`)
    SET 
      g.`DESC-RUBRO`=p.`DESC-RUBRO`,
      g.`DESC-PRODUCTO`=p.`DESC-PRODUCTO`,
      g.`DESC-MARCA`=p.`DESC-MARCA`;

  -- utilizando todas las tablas
  UPDATE gestion g
    JOIN marcas m USING(`COD-MARCA`)
    JOIN productos p USING(`COD-PRODUCTO`)
    JOIN rubros r USING(`COD-RUBRO`)
    SET
      g.`DESC-PRODUCTO`=p.`DESC-PRODUCTO`,
      g.`DESC-MARCA`=m.`DESC-MARCA`,
      g.`DESC-RUBRO`=r.`DESC-RUBRO`;




    

